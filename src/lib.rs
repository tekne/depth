use fxhash::FxHashMap as HashMap;
use na::Vector3;
use nphysics3d::force_generator::DefaultForceGeneratorSet;
use nphysics3d::joint::DefaultJointConstraintSet;
use nphysics3d::nalgebra as na;
use nphysics3d::object::{Body, DefaultBodySet, DefaultColliderSet, RigidBody};
use nphysics3d::world::{DefaultGeometricalWorld, DefaultMechanicalWorld};
use parking_lot::RwLock;
use smallvec::SmallVec;
use std::fmt::{self, Debug, Formatter};
use std::ops::Deref;
use std::sync::{Arc, Weak};
use std::time::Instant;
use ref_cast::RefCast;

/// A body ID
pub type BodyId = nphysics3d::object::DefaultBodyHandle;

/// The size of a chunk
pub const CHUNK_SIZE: usize = 16;

/// The size of a small number of blocks in a palette
pub const SMALL_PALETTE_SIZE: usize = 16;

/// A dimension handle
#[derive(Debug, Clone, RefCast)]
#[repr(transparent)]
pub struct DimensionHandle(pub Arc<RwLock<Dimension>>);

impl Deref for DimensionHandle {
    type Target = Arc<RwLock<Dimension>>;
    #[inline(always)]
    fn deref(&self) -> &Arc<RwLock<Dimension>> {
        &self.0
    }
}

impl From<Dimension> for DimensionHandle {
    fn from(dim: Dimension) -> DimensionHandle {
        DimensionHandle(Arc::new(RwLock::new(dim)))
    }
}

impl From<Arc<RwLock<Dimension>>> for DimensionHandle {
    fn from(dim: Arc<RwLock<Dimension>>) -> DimensionHandle {
        DimensionHandle(dim)
    }
}

/// A dimension
pub struct Dimension {
    /// The name of this dimension
    name: String,
    /// The root chunk tree in this dimension. This is generator-queried *every* tick, regardless of player position
    root_tree: Option<Arc<RwLock<ChunkTree>>>,
    /// The mechanical world of this dimension
    mechanical_world: DefaultMechanicalWorld<f64>,
    /// The geometrical world of this dimension
    geometrical_world: DefaultGeometricalWorld<f64>,
    /// The body set of this dimension
    bodies: DefaultBodySet<f64>,
    /// The collider set of this dimension
    colliders: DefaultColliderSet<f64>,
    /// The joint constraint set of this dimension
    joint_constraints: DefaultJointConstraintSet<f64>,
    /// The force generator set of this dimension
    force_generators: DefaultForceGeneratorSet<f64>,
}

impl Dimension {
    /// Create a new, empty dimension with a given name and gravity
    pub fn new(name: String, gravity: Vector3<f64>) -> Dimension {
        Dimension {
            name,
            root_tree: None,
            mechanical_world: DefaultMechanicalWorld::new(gravity),
            geometrical_world: DefaultGeometricalWorld::new(),
            bodies: DefaultBodySet::new(),
            colliders: DefaultColliderSet::new(),
            joint_constraints: DefaultJointConstraintSet::new(),
            force_generators: DefaultForceGeneratorSet::new(),
        }
    }
    /// Insert a body into this dimension
    pub fn insert_body(&mut self, body: impl Body<f64>) -> BodyId {
        self.bodies.insert(body)
    }
    /// Get a body in this dimension
    pub fn get_body(&self, body: BodyId) -> Option<&dyn Body<f64>> {
        self.bodies.get(body)
    }
    /// Get a rigid body in this dimension
    pub fn get_rigid(&self, body: BodyId) -> Option<&RigidBody<f64>> {
        self.bodies.get(body).map(Body::downcast_ref).flatten()
    }
    /// Forward a world's physics a tick
    pub fn physics_tick(&mut self) {
        self.mechanical_world.step(
            &mut self.geometrical_world,
            &mut self.bodies,
            &mut self.colliders,
            &mut self.joint_constraints,
            &mut self.force_generators,
        )
    }
}

impl Debug for Dimension {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        fmt.debug_struct("Dimension")
            .field("name", &self.name)
            .field("addr", &(self as *const _))
            .field("root_tree", &self.root_tree)
            .finish()
    }
}

/// A simulation agent
#[derive(Debug, Clone)]
pub struct Agent {
    /// The dimension this agent is in
    dim: DimensionHandle,
    /// The rigid body associated with this agent, if any
    body: BodyId,
    /// The chunks the center of mass of the agent is currently in *at this agent's scale*
    chunks: Vec<Arc<RwLock<Chunk>>>,
    /// The loading range of this agent
    load_range: f64,
}

impl Agent {
    /// Create a new agent with a given loading range in a given dimension with a given rigid body
    pub fn new(dim: DimensionHandle, body: BodyId, load_range: f64) -> Agent {
        Agent {
            dim,
            body,
            //TODO: this
            chunks: Vec::new(),
            load_range,
        }
    }
}

/// A chunk tree
#[derive(Debug, Clone)]
pub struct ChunkTree {
    /// The root chunk of this tree, if any has been generated yet
    root: Option<DimensionHandle>,
    /// The dimension this tree is in
    dim: Weak<RwLock<Dimension>>,
    /// The rigid body associated with this tree
    body: BodyId,
}

impl ChunkTree {
    /// Create a new, empty chunk tree in a given dimension with a given body
    pub fn new(dim: Weak<RwLock<Dimension>>, body: BodyId) -> ChunkTree {
        ChunkTree {
            root: None,
            dim,
            body,
        }
    }
}

/// A chunk
#[derive(Debug, Clone)]
pub struct Chunk {
    /// The data behind this chunk
    data: Arc<ChunkData>,
    /// The palette of this chunk
    palette: Arc<Palette>,
    /// The parent of this chunk, if any
    parent: Weak<RwLock<Chunk>>,
    /// The chunk tree this chunk is part of
    tree: Weak<ChunkTree>,
    /// The position of this chunk in it's parent
    parent_pos: Vector3<u8>,
    /// The relative position of this chunk to the origin
    rel_pos: Vector3<f64>,
    /// The logarithmic block-scale of this chunk
    log_scale: i8,
    /// The mass of this chunk
    mass: f64,
    /// The last instant this chunk was updated
    last_update: Instant,
}

impl Chunk {
    /// Create a new empty chunk at a given scale with a given parent and parent tree
    pub fn empty(log_scale: i8) -> Chunk {
        Chunk {
            data: Default::default(),
            palette: Default::default(),
            parent: Weak::default(),
            tree: Weak::default(),
            parent_pos: Vector3::zeros(),
            rel_pos: Vector3::zeros(),
            log_scale,
            mass: 0.0,
            last_update: Instant::now(),
        }
    }
}

/// The data behind a chunk
#[derive(Copy, Clone, Eq, PartialEq, Hash, Default)]
pub struct ChunkData {
    /// The indices composing this chunk
    pub ixes: [[[u16; CHUNK_SIZE]; CHUNK_SIZE]; CHUNK_SIZE],
}

impl Debug for ChunkData {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "<chunk_data@{:p}>", self)
    }
}

/// A palette of blocks, tile-entitites, and sub-chunks
#[derive(Clone, Default)]
pub struct Palette {
    blocks: SmallVec<[BlockId; SMALL_PALETTE_SIZE]>,
    chunks: SmallVec<[Arc<RwLock<Chunk>>; SMALL_PALETTE_SIZE]>,
    block_lookup: HashMap<u64, BlockId>,
}

impl Debug for Palette {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        fmt.debug_struct("Palette")
            .field("blocks", &self.blocks)
            .field("chunks", &self.chunks.len())
            .finish()
    }
}

/// A block ID
#[derive(Debug, Clone, Default)]
pub struct BlockId(u64);
