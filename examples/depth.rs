use depth::{Agent, Dimension, DimensionHandle};
use na::Vector3;
use nphysics3d::nalgebra as na;
use nphysics3d::object::RigidBodyDesc;

fn main() {
    let mut overworld = Dimension::new("Overworld".to_owned(), Vector3::new(0.0, -9.81, 0.0));
    eprintln!("Dim 0 = {:#?}", overworld);
    let player_rigid_body = RigidBodyDesc::new().set_mass(1.0).build();
    let player_body = overworld.insert_body(player_rigid_body);
    let overworld = DimensionHandle::from(overworld);
    let player = Agent::new(overworld.clone(), player_body, 100.0);
    eprintln!("Player 0 = {:#?}", player);
    let mut overworld = overworld.write();
    println!("y, vy");
    for _ in 0..1000 {
        let player_body = overworld.get_rigid(player_body).unwrap();
        println!(
            "{}, {}",
            player_body.position().translation.vector[1],
            player_body.velocity().linear[1]
        );
        overworld.physics_tick();
    }
}
